export function extractFirstOrg(organizations) {
  if (organizations && Object.keys(organizations).length > 0) {
    const firstOrgId = Object.keys(organizations)[0]
    return organizations[firstOrgId]
  }
  return {}
}


export function getConsumedServices(organizations,orgId) {
  const organization = orgId? organizations[orgId] : extractFirstOrg(organizations)
  let consumedServices = organization.consumed_services || []
  return consumedServices
}

export default {
  extractFirstOrg,
  getConsumedServices,
};