import React from "react";
import { Dropdown } from "react-bootstrap";
import "./style.css";
import {UserProfile} from "./userProfile.js";
import { getConsumedServices } from "../Services/utilities.js";
import queryString from "query-string";
import arrow from '../assets/images/icons/light-arrow.svg';
import Back from '../assets/images/icons/back.svg';
import _ from 'lodash';
import CommunityIcon from "../assets/images/icons/community-icon.svg";
import {COMMUNITY_URL, products } from "../common/constants";

const socketLogo='https://stuff.walkover.in/socketIconWhite.svg';
export class Header extends React.Component {
  state = {
    firstName: "",
    lastName: "",
    email: "",
  };
  componentDidMount() {}

  handleOpenLink(link, current=false){
    const {handleOpenLink} = this.props;
    if(!handleOpenLink)
      current ? window.open(link,'_self') : window.open(link,'_blank')
    else
      handleOpenLink(link);
  }

  navigateToHome() {
    const {organizations,productName,organizationId,history} = this.props;
    if(!_.isEmpty(organizations)){
      const orgId = organizationId
      let pathname = `/orgs/${orgId}/${productName}`
      let consumedServices = getConsumedServices(organizations,orgId)
      if (consumedServices.includes(productName)) {
        pathname = `/orgs/${orgId}/projects`
      }
      history.push({
        pathname,
      });
    }
  }

  navigateToProductDashboard() {
    const {organizationId,history} = this.props;
    history.push({
      pathname: `/orgs/${organizationId}/products/`,
    });
  }  

  renderNavbarBrand() {
    return (
       <React.Fragment>
          <div className="navbar-logo">
            <img
              style={{ cursor: "pointer" }}
              src = {socketLogo}
              alt="viasocket-icon"
              width="22px"
              height="auto"
              onClick={() => this.navigateToHome()}
            />
          </div>
      </React.Fragment>
    );
  }


  switchProduct(product){
    const { organizationId,productLinks }=this.props;
    let orgId = organizationId;
    let link = productLinks[product.toUpperCase()];

    if(product!==products.HTTPDUMP){
     link+=`/orgs/${orgId}`
    }
    this.handleOpenLink(link)
  }
  
  renderLeftNavigation(){
    const {location,showDashboardButton,productName} = this.props;
    const pathname = location.pathname;
    this.product = queryString.parse(location.search).product
    let isProjectDashboard = pathname.includes("/products")
    return (<>
      {showDashboardButton&&<div className='show-dashboard text-white d-flex justify-content-center ' onClick={() => { this.navigateToProductDashboard()}}><img height={'16px'} width={'16px'} src={arrow} alt='' class="mr-10 rotate-270" />Go to Product Dashboard</div>}
      {
        !isProjectDashboard && <div className='switchProduct m-1r'>
          {!showDashboardButton&&<Dropdown>
            <Dropdown.Toggle variant='success' id='dropdown-basic' className="d-flex align-items-center" drop="end">
            {!this.product && this.renderNavbarBrand()} <span class="text-uppercase"> {productName} </span> <img src={arrow} />
            </Dropdown.Toggle>
            {this.renderSwitchProducts()}
          </Dropdown>}
        </div>
      }
    </>);
  }

  renderSwitchProducts(){
    const { productName:currProductName }=this.props;
    return(
      <Dropdown.Menu >
        <Dropdown.Item href='#' className='dropHeader'>
          Switch to
        </Dropdown.Item>
        {Object.keys(products).map((productName)=>{
          const product=products[productName];
          return(
            <div className="text-capitalize">
              {currProductName!=product&&<Dropdown.Item href='#' onClick={() => { this.switchProduct(product)}}>
                {product}
              </Dropdown.Item>}
            </div>
          );
        })}
        {currProductName==products.EBL && <Dropdown.Item href='' onClick={() => { this.navigateToProductDashboard() }} className="go-to-link">
          <img height={'16px'} width={'16px'} src={arrow} alt='' class="mr-10 rotate-270" />
          Go to Product dashboard
        </Dropdown.Item>}
      </Dropdown.Menu>
    );
  }

  renderProjectName() {
    let {project_name:projectName} = this.props;
    return (
      projectName && <div  className="link-new d-flex align-items-center" onClick={() => { this.navigateToHome()} }> 
        <img src={Back} className="mr-10" />
        {projectName}
      </div> 
    )
  }

  handleGoBack(){
    const { history, location } = this.props;
    const urlParams = new URLSearchParams(location.search);
    const redirectUri = urlParams.get('redirect_uri');
    if(redirectUri) this.handleOpenLink(redirectUri,true)
    else history.goBack()
    
  }
  
  renderBackOption(){
    const { location } = this.props;
    let pathArray = location.pathname.split('/');
    return(
      (pathArray[3]==="manage"||pathArray[3]==="authKeys"|| pathArray[3]==="billing") && <div className="link-new d-flex align-items-center black-hover-i" onClick={() => this.handleGoBack()}> 
        <img src={Back} className="" />
        {" Back"}
      </div> 
    )
  }

  renderMidPortion(){
    return <>{this.props?.renderNavTitle()}</>;
  }

  renderCommunityButton(){
    const {showCommunityButton} = this.props;
    return (
      <>
        {showCommunityButton&&<div
          className="ml-2 d-flex align-items-center p-2 black-hover mr-10 transition"
          onClick={() => {
            this.handleOpenLink(COMMUNITY_URL)}}
          >
          <img src={CommunityIcon}></img>
        </div>}
      </>
    );
  }
  
  renderUserInfo(){
    const {renderLoginButton} = this.props;
    return (
      <>
        {(!renderLoginButton||renderLoginButton()=="")?
          <UserProfile {...this.props}></UserProfile>
        :renderLoginButton()}
      </>
    );
  }


  render() {
    const {location} = this.props;
    const pathname = location.pathname;
    this.product = queryString.parse(location.search).product
    let isProjectDashboard = pathname.includes("/products")
    return (
      <React.Fragment>
        <div className={['common-header',isProjectDashboard? 'headerNavbarWhite' : ''].join(" ")}>
          <div className="d-flex">
            <div className="logoBox d-flex align-items-center">
              <div className="brand-block justify-content-left align-items-center black-hover-it">
                {this.renderLeftNavigation()}
              </div>
              {!this.product && this.renderProjectName()}
              {this.renderBackOption()}
            </div>
            <div className="header-mid-portion d-flex">
              {this.renderMidPortion()}
            </div>
            <div className="d-flex justify-content-end header-right-portion">
                {this.renderCommunityButton()}
                {this.renderUserInfo()}
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
