import React from "react";
import { Nav, Dropdown } from "react-bootstrap";
import Avatar from "react-avatar";
import lightArrow from '../assets/images/icons/light-arrow.svg'
import { FixedSizeList as List } from 'react-window';
import Users from '../assets/images/icons/users.svg';
import User from '../assets/images/icons/user.svg';
import Power from '../assets/images/icons/power.svg';
import Settings from '../assets/images/icons/settings.svg';
import File from '../assets/images/icons/file.svg';
import Other from '../assets/images/icons/file.svg';
import RightArrow from '../assets/images/icons/right-arrow.svg';
import { products } from "../common/constants";
export class UserProfile extends React.Component {
  state = {
    firstName: "",
    lastName: "",
    email: "",
    orgFilter: "",
    moreFlag: false,
  };

  returnName() {
    const {profile} = this.props;
    let firstName = profile?.first_name || "";
    let name = firstName ;
    let email =profile?.email||"";
    return name===" "?email:name;
  }

  openAccountAndSettings() {
    const {productName,history,organizationId,location}=this.props;
    if(productName!=products.EBL)
      this.openOptions("/manage/users")
    else
      history.push({
        pathname: `/orgs/${organizationId}/manage`,
        search: location.search
      })
  }

  renderBilling(){
    const {productName,history,organizationId} = this.props;
    return(
      <div class="profile-listing" onClick={()=>{
        if(productName==products.EBL){
          history.push({
            pathname: `/orgs/${organizationId}/billing`
          })
        }else{
          this.openOptions(`/billing/subscription/${productName}`);
        } 
      }}>
        <img src={File} />
        <span className="label">Billing</span>
      </div>
    )
  }

  renderSettings(){
    const {productName,history,organizationId} = this.props;
    return(
      <div class="profile-listing" onClick={()=>history.push({
        pathname: productName==products.CONTENTBASE?`/orgs/${organizationId}/authKeys`:`/orgs/${organizationId}/manage/authkeys`
      })}>
        <img src={Settings} />
        <span className="label">Auth Keys</span>
      </div>
    )
  }

  openOptions(path) {
    const {match,productName,handleOpenLink} = this.props;
    const viasocketUrl=process.env.REACT_APP_VIASOCKET_URL;
    const currProductUrl=process.env.REACT_APP_UI_BASE_URL||process.env.REACT_APP_UI_URL
    const { orgId } = match.params;
    if (orgId) {
      let url = `${viasocketUrl}/orgs/${orgId}${path}?product=${productName}`;
      if (path === "/products") {
        url += "";
      } else {
        url += `&redirect_uri=${currProductUrl}`;
      }
      if(!handleOpenLink)
        window.open(url, "_blank");
      else
        handleOpenLink(url);
    }else{
      console.log("Organization ID not found")
    }
  }

  renderOtherProducts(){
    return(
      <div class="profile-listing" onClick={()=>{
        this.openOptions("/products");
      }}>
        <img src={Other} />
        <span className="label">Other Products</span>
      </div>
    )
  }

  getCurrentOrg(){
    let filteredOrgsArray = this.getAllOrgs();
    filteredOrgsArray=filteredOrgsArray.filter((org)=> !this.compareOrg(org))
    return filteredOrgsArray[0];
  }

  renderAvatarWithOrg(onClick,ref1){
    const {getNotificationCount} = this.props;
    return(
      <div className="menu-trigger-box black-hover-it" onClick={(e) => {
        e.preventDefault();
        onClick(e);
      }}>
        <Avatar color={'#343a40'} name={this.returnName()} size={24} round="30px" />
        <img
          ref={ref1}
          src={lightArrow}
          alt="settings-gear"
          className="transition"
        />
        {getNotificationCount&&getNotificationCount() > 0 &&
            <span className='user-notification-badge'>{getNotificationCount()}</span>}
      </div>
    )
  }

  getUserDetails(){
    const {profile} = this.props;
    let firstName = profile?.first_name || "";
    let lastName = profile?.last_name || "";
    let name = firstName + " " + lastName;
    let email = profile?.email;
    return {email,name}
  }

  getAllOrgs(){
    const orgsArray = Object.values(this.props.organizations || {})
    const { orgFilter } = this.state
    const filteredOrgsArray = orgsArray.filter(org => 
      org.name.toLowerCase().includes(orgFilter.toLowerCase()
    ))
    .sort((a,b)=>{
      if(a.name.toLowerCase() < b.name.toLowerCase()) return -1;
      else if (a.name.toLowerCase() > b.name.toLowerCase()) return 1;
      else return 0;
    })

    return filteredOrgsArray
  }

  setShowFlag(){
    let orgFilter = this.state.orgFilter
    let moreFlag = !this.state.moreFlag;
    if(!moreFlag){
      orgFilter = ""
    }
    this.setState({orgFilter,moreFlag})
  }

  setOrgFilter(orgFilter){
    this.setState({ orgFilter})
  }

  getItemCount(orgCount){
    let showFlag = this.state.moreFlag;
    if(orgCount>5 && !showFlag){
      return 5;
    }else{
      return orgCount;
    }
  }

  switchOrgRoute(id){
    const {history} = this.props;
    let pathname="";
    let pathArray = history.location.pathname.split('/');
    if (pathArray[3]==='manage') {
      pathname = `/orgs/${id}/manage`
    } else {
      pathname= `/orgs/${id}`;
    }
    history.push({
      pathname
    });
  }
  compareOrg(org){
    const {productName,organizationId} = this.props;
    if(productName!=products.EBL){
      return org.identifier!==organizationId;
    }else{
      return org.id!==organizationId;
    }
  }
  renderOrgList(){
    const {organizations,productName} = this.props;
    const orgsLength = Object.keys(organizations || {})?.length;
    let filteredOrgsArray = this.getAllOrgs();
    filteredOrgsArray=filteredOrgsArray.filter((org)=> this.compareOrg(org))
    const orgItem = ({ index, style }) => {
      const item = filteredOrgsArray[index];
      return (
        <Dropdown.Item style={style}>
          <div 
            key={item?.id} 
            className='org-listing'  
            onClick={()=>
              {
                const currentId=productName==products.EBL?item?.id:item?.identifier;
                this.props.switchOrg(currentId);
              }}
          >
            <span className="org-listing-name">{item?.name}</span>
            <img src={RightArrow} />
          </div>  
        </Dropdown.Item>
      )
    }

    return(
      ( orgsLength > 1 && <div className="OrgsBlock">
        <div className="text-uppercase text-sm-bold">SWITCH ORGS</div>
        <div className='orgs-listing-container'>
          {this.state.moreFlag && 
          <div className="p-2 search-profile">
            <input 
              className='form-control'
              onChange={(e)=>this.setOrgFilter(e.target.value,filteredOrgsArray?.length||0)}
              value={this.state.orgFilter}
              placeholder='Search'
            />
          </div>
          }
        {filteredOrgsArray.length == 0 
          ? <div className='pb-2 text-center w-100'><small className='body-6'>No Organizations Found</small></div>
          : <List height={filteredOrgsArray.length < 5 ? 36*filteredOrgsArray.length : 180} itemCount={this.getItemCount(filteredOrgsArray.length)} itemSize={35}>
              {orgItem}
            </List>}
      </div>
      {orgsLength > 5 &&
       <div className="ShowMore text-center" onClick={()=>this.setShowFlag()}>
         {!this.state.moreFlag ? "Show more" : "Show less"}
       </div>}
      </div>
      )
    )
  }
  renderMenuButton(){
    return this.props?.renderProfileOption();
  }

  renderInviteTeams(){
    return (
      <div class="profile-listing" onClick={() => { this.openAccountAndSettings() }}>
        <img src={Users} />
        <span className="label">{"Invite Team"}</span>
      </div>
    );
  }

  renderLogout(){
    return (
      <div className="profile-listing" onClick={() => { this.props.history.push({
        pathname: '/logout'
      }) }}>
        <img src={Power} />
        <span class="label">Logout</span>
      </div>
    );
  }

  renderOrgName(){
    return (
      <div className="text-center org-name">
        {this.getCurrentOrg()?.name || null}
      </div>
    );
  }

  renderUserDetails(){
    const {name,email} = this.getUserDetails();
    return (
      <div
        className="profile-details d-flex align-items-center"
        onClick={() => { }}
      >
          <div class="user-icon">
            <img src={User} />
          </div>
          <div className="profile-details-user-name">
            <span class="profile-details-label">{name}</span>
            <span class="profile-details-label-light">{email}</span>
          </div>
      </div>
    );
  }

  render() {
    const {productName} = this.props;
    return (
      <Nav>
        <React.Fragment>
          <Dropdown className="menu-dropdown transition d-flex align-items-center">
            <Dropdown.Toggle
              as={React.forwardRef(({ children, onClick }, ref1) => (
                this.renderAvatarWithOrg(onClick,ref1)
              ))}
              id="dropdown-custom-components"
            ></Dropdown.Toggle>
            <Dropdown.Menu>
              {this.renderOrgName()}
              {this.renderUserDetails()}   
              <div className="profile-listing-container">
                {this.renderMenuButton()}
                <Dropdown.Item>{this.renderInviteTeams()}</Dropdown.Item>
                <Dropdown.Item>{this.renderBilling()} </Dropdown.Item>
                <Dropdown.Item>{productName!=products.HITMAN&&this.renderSettings()} </Dropdown.Item>
                <Dropdown.Item>{productName!=products.EBL&&this.renderOtherProducts()}</Dropdown.Item>
                <Dropdown.Item>{this.renderLogout()}</Dropdown.Item>
              </div>
              {this.renderOrgList()}
            </Dropdown.Menu>
          </Dropdown>
        </React.Fragment>
      </Nav>
    );
  }
}