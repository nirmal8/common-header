//This data is to be used while testing the code in storybook

export const data = {
  organizationId: "WsNymZ7wb5mu1LbrHj2G",
  organizations: {
    WsNymZ7wb5mu1LbrHj2G: {
      id: "WsNymZ7wb5mu1LbrHj2G",
      name: "Walkover",
      domain: "walkover",
      created_by: {
        id: "hHNxfHC6T78orDux1yPx",
        email: "sourabh@walkover.in",
      },
      updated_by: {
        id: "hHNxfHC6T78orDux1yPx",
        email: "sourabh@walkover.in",
      },
      consumed_services: [
        "ebl",
        "hitman",
        "superform",
        "sheetasdb",
        "feedio",
        "httpdump",
      ],
      feedback_form_filled: true,
    },
    YBTMbadJnASh2X8Vxyjf: {
      id: "YBTMbadJnASh2X8Vxyjf",
      name: "mahagram",
      domain: "ram-mahagram-in",
      created_by: {
        id: "Huby7GbJzsrZnGFLJq2q",
        email: "ram@mahagram.in",
      },
      updated_by: {
        id: "Huby7GbJzsrZnGFLJq2q",
        email: "ram@mahagram.in",
      },
      consumed_services: ["ebl"],
      feedback_form_filled: false,
    },
    LAweEhUf6gZer31TdflR: {
      id: "LAweEhUf6gZer31TdflR",
      name: "red-id",
      domain: "ram-red-id-com",
      created_by: {
        id: "D6qfZxsTeyajAJavrR3t",
        email: "ram@red-id.com",
      },
      updated_by: {
        id: "D6qfZxsTeyajAJavrR3t",
        email: "ram@red-id.com",
      },
      consumed_services: ["ebl"],
      feedback_form_filled: false,
    },
    FLw3YSxtkZjw74RaEc7N: {
      id: "FLw3YSxtkZjw74RaEc7N",
      name: "optiweb",
      domain: "sales-optiweb-ca",
      created_by: {
        id: "T7mFeEMcy32ZsM4t98YM",
        email: "sales@optiweb.ca",
      },
      updated_by: {
        id: "T7mFeEMcy32ZsM4t98YM",
        email: "sales@optiweb.ca",
      },
      consumed_services: ["ebl"],
      feedback_form_filled: false,
    },
    esF7HBb1uVAAq7CPZAJK: {
      id: "esF7HBb1uVAAq7CPZAJK",
      name: "sawada",
      domain: "sales-sawada-com-my",
      created_by: {
        id: "KkwUo8E3yxEyAAQRosxS",
        email: "sales@sawada.com.my",
      },
      updated_by: {
        id: "KkwUo8E3yxEyAAQRosxS",
        email: "sales@sawada.com.my",
      },
      consumed_services: ["ebl"],
      feedback_form_filled: false,
    },
    yxsLnJzdCO6ThY1lIGAn: {
      id: "yxsLnJzdCO6ThY1lIGAn",
      name: "scottopic",
      domain: "scott-scottopic-com",
      created_by: {
        id: "o46nf5VyruosYsLm41uq",
        email: "scott@scottopic.com",
      },
      updated_by: {
        id: "o46nf5VyruosYsLm41uq",
        email: "scott@scottopic.com",
      },
      consumed_services: ["ebl"],
      feedback_form_filled: false,
    },
  },
  profile: {
    email: "sojivoc135@wwmails.com",
    first_name: "Aleksander",
    last_name: "West",
    country_code: "+42",
    contact_number: "0605118029",
    tour_step: 5,
    identifier: "uP7HionCmFn45NwFGqcB",
    company: "AlekINc.",
    designation: "Software Developer",
    admin: true,
    signed_up_at: "2020-08-04T04:13:24.314Z",
    last_sign_in_at: "2021-11-20T09:53:18.234Z",
    created_at: "2020-07-04T04:13:24.314Z",
  },
  history: {
    length: 2,
    action: "PUSH",
    location: {
      pathname: "/orgs/WsNymZ7wb5mu1LbrHj2G/projects",
      search: "",
      hash: "",
      key: "xpp3u8",
    },
  },
  match: {
    path: "/orgs/:orgId",
    url: "/orgs/WsNymZ7wb5mu1LbrHj2G",
    isExact: false,
    params: {
      orgId: "WsNymZ7wb5mu1LbrHj2G",
    },
  },
  location:{
    "pathname": "/orgs/WsNymZ7wb5mu1LbrHj2G/projects",
    "search": "",
    "hash": "",
    "key": "xpp3u8"
  },
  productName:"ebl",
  project_name:"projectName",
  renderNavTitle:()=>{},
  renderProfileOption:()=>{},
  showCommunityButton:true
};