import React, { Component } from 'react';
import {storiesOf} from '@storybook/react';
import {Header} from '../components/Header';
import {data} from '../components/common/dummyData'
import './bootstrap.css'
const stories = storiesOf('App Test', module);
stories.add('App', ()=>{
  return <Header {...data}/>
});