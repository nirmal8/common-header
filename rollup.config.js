import babel from "rollup-plugin-babel";
import resolve from "@rollup/plugin-node-resolve";
import external from "rollup-plugin-peer-deps-external";
import postcss from "rollup-plugin-postcss";
import commonjs from "@rollup/plugin-commonjs";
import image from '@rollup/plugin-image';

export default [
  {
    input: "./src/index.js",
    output: [
      {
        file: "dist/index.js",
        format: "cjs",
      },
      {
        file: "dist/index.es.js",
        format: "es",
        exports: "named",
      },
    ],
    plugins: [
      commonjs({
        include: /node_modules/,
        namedExports: {
          react: ["useState", "useEffect","React","Component"],
          "@apollo/client": [
            "ApolloProvider",
            "ApolloClient",
            "HttpLink",
            "InMemoryCache",
            "useQuery",
            "gql",
          ],
          "styled-components": ["styled", "css", "ThemeProvider"],
        },
      }),
      babel({
        exclude: "node_modules/**",
        presets: ["@babel/preset-react"],
      }),
      postcss({
        inject:true
      }),
      external(),
      resolve(),
      image(),
    ],
  },
];
